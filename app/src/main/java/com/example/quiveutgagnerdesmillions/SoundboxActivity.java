package com.example.quiveutgagnerdesmillions;

import android.app.ActionBar;
import android.content.Intent;
import android.media.MediaPlayer;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

public class SoundboxActivity extends AppCompatActivity implements View.OnClickListener {
    int currentPos = 0;
    //Themes
    private MediaPlayer main_theme_Player = null;
    //Answers
    private MediaPlayer answer_Player = null;
    //Jokers
    private MediaPlayer joker_Player = null;
    //Paliers
    private MediaPlayer palier_Player = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_soundbox);

        Button btn_sound_main_theme = findViewById(R.id.btn_sound_main_theme);
        Button btn_reset = findViewById(R.id.btn_reset);
        //Answer
        Button btn_sound_answer_final = findViewById(R.id.btn_sound_answer_final);
        Button btn_sound_answer_wrong = findViewById(R.id.btn_sound_answer_wrong);
        Button btn_sound_answer_correct = findViewById(R.id.btn_sound_answer_correct);
        //Jokers
        Button btn_sound_joker_50 = findViewById(R.id.btn_sound_joker_50);
        Button btn_sound_joker_friend = findViewById(R.id.btn_sound_joker_friend);
        Button btn_sound_joker_audience = findViewById(R.id.btn_sound_joker_audience);
        //Paliers
        Button btn_sound_palier_1 = findViewById(R.id.btn_sound_palier_1);
        Button btn_sound_palier_2 = findViewById(R.id.btn_sound_palier_2);
        Button btn_sound_palier_3 = findViewById(R.id.btn_sound_palier_3);
        Button btn_sound_palier_4 = findViewById(R.id.btn_sound_palier_4);
        Button btn_sound_palier_5 = findViewById(R.id.btn_sound_palier_5);
        Button btn_sound_palier_6 = findViewById(R.id.btn_sound_palier_6);


        //btn_sound_main_theme.setOnClickListener(new OnClickListener() {
        btn_sound_main_theme.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        //Answers
        btn_sound_answer_final.setOnClickListener(this);
        btn_sound_answer_wrong.setOnClickListener(this);
        btn_sound_answer_correct.setOnClickListener(this);
        //Jokers
        btn_sound_joker_50.setOnClickListener(this);
        btn_sound_joker_friend.setOnClickListener(this);
        btn_sound_joker_audience.setOnClickListener(this);
        //Paliers
        btn_sound_palier_1.setOnClickListener(this);
        btn_sound_palier_2.setOnClickListener(this);
        btn_sound_palier_3.setOnClickListener(this);
        btn_sound_palier_4.setOnClickListener(this);
        btn_sound_palier_5.setOnClickListener(this);
        btn_sound_palier_6.setOnClickListener(this);

        main_theme_Player = MediaPlayer.create(this, R.raw.main_theme);

        // boutton retour
        //getActionBar().setDisplayHomeAsUpEnabled(true);
    }
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sound_main_theme:
                if (main_theme_Player.isPlaying()) {
                    main_theme_Player.pause();
                } else {
                    main_theme_Player.start();
                }
                break;
            // ------> Answer
            case R.id.btn_sound_answer_final:
                //Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                play_answer("answer_final");
                break;
            case R.id.btn_sound_answer_correct:
                //Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                play_answer("answer_correct");
                //relance_palier_player();
                break;
            case R.id.btn_sound_answer_wrong:
                //Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                play_answer("answer_wrong");
                break;
            // ------> Joker
            case R.id.btn_sound_joker_50:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_joker("joker_50");
                break;
            case R.id.btn_sound_joker_friend:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_joker("joker_friend");
                break;
            case R.id.btn_sound_joker_audience:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_joker("joker_audience");
                break;
            // ------> Palier
            case R.id.btn_sound_palier_1:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_palier("palier_1_100_1000");
                break;
            case R.id.btn_sound_palier_2:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_palier("palier_2_2k_32k");
                break;
            case R.id.btn_sound_palier_3:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_palier("palier_3_64k");
                break;
            case R.id.btn_sound_palier_4:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_palier("palier_4_125k_250k");
                break;
            case R.id.btn_sound_palier_5:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_palier("palier_5_500k");
                break;
            case R.id.btn_sound_palier_6:
                //Toast.makeText(this, "Button 4 clicked", Toast.LENGTH_SHORT).show();
                play_palier("palier_6_1m");
                break;

            case R.id.btn_reset:
                kill_all_player();
                break;

        }
    }

    public void kill_all_player() {
        if (answer_Player != null) {
            answer_Player.stop();
            answer_Player.release();
            answer_Player = null;
        }
        if (palier_Player != null) {
            palier_Player.stop();
            palier_Player.release();
            palier_Player = null;
        }
        if (joker_Player != null) {
            joker_Player.stop();
            joker_Player.release();
            joker_Player = null;
        }
        if (main_theme_Player != null) {
            main_theme_Player.stop();
            main_theme_Player.release();
            main_theme_Player = null;
        }
    }

    public void relance_palier_player() {
        if (palier_Player.isPlaying()){
            currentPos = palier_Player.getCurrentPosition();
            palier_Player.pause();
        }else {
            palier_Player.seekTo(currentPos);
            palier_Player.start();
        }
    }

    public void play_answer(String nameOfFile) {

        if (palier_Player != null){
            palier_Player.pause();
        }
        if (answer_Player != null) {
            answer_Player.release();
            answer_Player = null;
        }
        if (joker_Player != null) {
            joker_Player.release();
            joker_Player = null;
        }

        answer_Player = MediaPlayer.create(SoundboxActivity.this, getResources().getIdentifier(nameOfFile, "raw", getPackageName()));
        answer_Player.start();
        if (nameOfFile.equals("answer_correct")){
            palier_Player.start();
        }
    }
    public void play_palier(String nameOfFile) {

        if (palier_Player != null) {
            palier_Player.release();
            palier_Player = null;
        }
        palier_Player = MediaPlayer.create(SoundboxActivity.this, getResources().getIdentifier(nameOfFile, "raw", getPackageName()));
        palier_Player.start();
        palier_Player.setLooping(true);
    }

    /*
    * On interromp les musiques de palier pour les jokers "Avis du public" et "Appel à un ami"
     */
    public void play_joker(String nameOfFile) {
        if (joker_Player != null) {
            joker_Player.release();
            joker_Player = null;
        }
        if (!nameOfFile.equals("joker_50")) {
            if (palier_Player != null){
                palier_Player.pause();
            }
        }
        joker_Player = MediaPlayer.create(SoundboxActivity.this,
                getResources().getIdentifier(nameOfFile,
                        "raw", getPackageName()));
        joker_Player.start();

    }

}
/*
    @Override
    public void onPause() {
        super.onPause();
        if(mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
        }
    }

    private void playSound(int resId) {
        if(mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
        }
        //mPlayer = MediaPlayer.create(this, resId);
        mPlayer.start();
    }
*/
