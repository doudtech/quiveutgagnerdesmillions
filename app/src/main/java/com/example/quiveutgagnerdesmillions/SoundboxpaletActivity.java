package com.example.quiveutgagnerdesmillions;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class SoundboxpaletActivity extends AppCompatActivity implements View.OnClickListener {

    private MediaPlayer sound_Player = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soundboxpalet);

        // Menu
        Button btn_reset = (Button) findViewById(R.id.btn_reset);
        Button btn_intro = (Button) findViewById(R.id.btn_intro);
        // Gilardi
        Button btn_sound_vasypetit = (Button) findViewById(R.id.btn_sound_vasypetit);
        Button btn_sound_ilestgeniallemome = (Button) findViewById(R.id.btn_sound_ilestgeniallemome);
        Button btn_sound_pascazinedine = (Button) findViewById(R.id.btn_sound_pascazinedine);
        //Da Fonseca
        Button btn_sound_soufflage = (Button) findViewById(R.id.btn_sound_soufflage);
        //Thierry Roland
        Button btn_sound_quelpied = (Button) findViewById(R.id.btn_sound_quelpied);
        //PEL
        Button btn_sound_ceseraitbiendeconcure = (Button) findViewById(R.id.btn_sound_ceseraitbiendeconcure);

        btn_reset.setOnClickListener(this);
        btn_intro.setOnClickListener(this);
        btn_sound_vasypetit.setOnClickListener(this);
        btn_sound_ilestgeniallemome.setOnClickListener(this);
        btn_sound_pascazinedine.setOnClickListener(this);
        btn_sound_soufflage.setOnClickListener(this);
        btn_sound_quelpied.setOnClickListener(this);
        btn_sound_ceseraitbiendeconcure.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_reset:
                kill_all_player();
                break;
            case R.id.btn_intro:
                play_sound("intro");
                break;
            case R.id.btn_sound_vasypetit:
                play_sound("gilardi_vasypetit");
                break;
            case R.id.btn_sound_ilestgeniallemome:
                play_sound("gilardi_ilestgeniallemome");
                break;
            case R.id.btn_sound_pascazinedine:
                play_sound("gilardi_pascazinedine");
                break;
            case R.id.btn_sound_soufflage:
                play_sound("dafonseca_soufflage");
                break;
            case R.id.btn_sound_quelpied:
                play_sound("roland_quel_pied_putain");
                break;
            case R.id.btn_sound_ceseraitbiendeconcure:
                play_sound("pel_caseraitbiendeconclure");
                break;
        }
    }
    public void kill_all_player() {
        if (sound_Player != null) {
            sound_Player.stop();
            sound_Player.release();
            sound_Player = null;
        }
    }

    public void play_sound(String nameOfFile) {
        if (sound_Player != null) {
            sound_Player.release();
            sound_Player = null;
        }
        sound_Player = MediaPlayer.create(SoundboxpaletActivity.this,
                getResources().getIdentifier(nameOfFile,
                        "raw", getPackageName()));
        sound_Player.start();

    }

}
