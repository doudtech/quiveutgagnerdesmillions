package com.example.quiveutgagnerdesmillions;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView play = findViewById(R.id.btn_qvgdm);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), SoundboxActivity.class);
                startActivity(otherActivity);
                finish();
            }
        });

        Button btn_boxpalet = findViewById(R.id.btn_boxpalet);

        btn_boxpalet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), SoundboxpaletActivity.class);
                startActivity(otherActivity);
                finish();
            }
        });

        Button btn_rtsButton = findViewById(R.id.btn_ipcam);

        btn_rtsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*RtspActivities
                Intent otherActivity = new Intent(getApplicationContext(), RtspActivity.class);
                startActivity(otherActivity);*/

                // Lance une autre appli
                // gmail = com.google.android.gm
                // ipcam = com.pas.webcam
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.pas.webcam");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
                finish();
            }
        });

    }
}

